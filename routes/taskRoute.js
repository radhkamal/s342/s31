// Contains all of the endpoints of our application

// we need to use express's Router() function to achieve this
const express = require("express");

// allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();

// allows us to use the functions defined in the taskController.js
const taskController = require("../controllers/taskController");

// Route to get all the tasks
router.get("/", (request, response) => {
	taskController.getAllTasks().then(resultFromController => response.send(resultFromController));
});

// router to create a task
router.post("/", (request, response) => {

	console.log(request.body);
// ======================================2
	taskController.createTask(request.body).then(resultFromController => response.send(resultFromController));
})

// router for deleting a task
router.delete("/:id", (request, response) => {
	console.log(request.params);
	taskController.deleteTask(request.params.id).then(resultFromController=> response.send(resultFromController));
})

// router to update a task
router.put("/:id", (request, response) => {
	taskController.updateTask(request.params.id, request.body).then(resultFromController=> response.send(resultFromController));
})

//============================1. Create a route for getting a specific task.==============================//

// route to get specific task
router.get("/:id", (request, response) => {
	taskController.getSpecifiedTask(request.params.id).then(resultFromController=> response.send(resultFromController));
});


// ======================5. Create a route for changing the status of a task to "complete".===================//
router.put("/:id/complete", (request, response) => {
	taskController.updateStatus(request.params.id, request.body).then(resultFromController=> response.send(resultFromController));
})


// use "module.exports" to export the router object to use in the index.js
module.exports = router;