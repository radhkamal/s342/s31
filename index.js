// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Database Connection
mongoose.connect("mongodb+srv://admin:admin123@course-booking.qxq86.mongodb.net/B157_To-Do?retryWrites=true&w=majority", 
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the cloud database!"));

// add the task route
// allow all the task routes created in the "taskRoute.js" to use the "/tasks" route
app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`Now listening to port ${port}`));


/*

Instructions:
1. Create a route for getting a specific task.
2. Create a controller function for retrieving a specific task.
3. Return the result back to the client/Postman.
4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
5. Create a route for changing the status of a task to "complete".
6. Create a controller function for changing the status of a task to "complete".
7. Return the result back to the client/Postman.
8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
9. Create a git repository named S31.
10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
11. Add the link in Boodle.

*/