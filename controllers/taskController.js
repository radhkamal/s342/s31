// Controllers contain the functions and business logic of our JS application
const Task = require("../models/task")

// Controller function for getting all the tasks
// Defines functions to be used to be used in taskRoutes.js file
module.exports.getAllTasks = () => {

	// model.method
	return Task.find({}).then(result => {
		return result;
	})
}

// controller function for creating task
module.exports.createTask = (requestBody) => {

	let newTask = new Task ({
		name: requestBody.name
	})


//=======================1
// saves the newly created "newTask" in our MongoDB database
// The "then" method waits until the task is stored in the database or an error is encountered before returning a "true" or "false" value back to the client
// the "then" method will accept 2 arguments
	// first parameter will store the result returned by the Mongoose "save" method
	// second parameter will store the "error" object
	return newTask.save().then((task, error) => {
		if (error) {
			console.log(error);
			return false
		} else {
			return task;
		}
	})
}


// Controller function for deleting a task
module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {

		if (err) {

			console.log(err);
			return false;

		} else {

			return removedTask;
		}
	})
}

// controller function for updating a task
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if (error) {
			console.log(error);
			return false;
		}

		result.name = newContent.name;
		return result.save().then((updatedTask, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
				return false;
			}

			else {
				return updatedTask;
			}
		})
	})
}


//====================2. Create a controller function for retrieving a specific task.=========================//

module.exports.getSpecifiedTask = (taskId) => {

	return Task.findById(taskId).then((specifiedTask, error) => {
		if (error) {
			console.log(error);
			return false;

		} else {
				return specifiedTask;
			}
		})
	}

//===================6. Create a controller function for changing the status of a task to "complete".=============//

module.exports.updateStatus = (taskId) => {
	return Task.findById(taskId).then((result, error) => {
		if (error) {
			console.log(error);
			return false;
		}

		let status = "complete";
		result.status = status;
		return result.save().then((updatedStatus, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
				return false;
			}

			else {
				return updatedStatus;
			}
		})
	})
}